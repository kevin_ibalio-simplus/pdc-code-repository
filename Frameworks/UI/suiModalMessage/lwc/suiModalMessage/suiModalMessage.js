import { LightningElement, api, track } from 'lwc';

const buttonYesNo = [{label:'No',type:'neutral',isClose:true},{label:'Yes',type:'positive'}];
const buttonClose = [{label:'Close',type:'neutral',isClose:true}];
const buttonOkay = [{label:'Okay',type:'neutral',isClose:true}];
const buttonCancelNext = [{label:'Cancel',type:'neutral'},{label:'Next',type:'positive'}];
export default class SuiModalMessage extends LightningElement {
    id;
    @api show(param) {
        this.doDisplayMessage(param);
    }
    preventScroll = false;

    isDisplayed = false;
    isMobile = false;
    isGiant = false;
    isMini = true;

    type; 
    @track buttonSet = [];
    
    hasClose = false;
    hasIcon = false;
    headerIconName = "utility:info_alt";
    messageIconName = "utility:info_alt";

    messageHeader = "Message";
    messageTag = "Message";
    messageBody = "Message Body";


    topLevelClassName = 'suiModalBackdrop';
    footerClassName = 'suiModalControls';

    passBack;
    

    @api display(id,param) {
        this.topLevelClassName = this.util_getTopLevelClass();
        this.util_reset();
        this.id = id;
        if(param.type) {
            this.type = param.type.toLowerCase();
            this.passBack = param.passBack;
            if(param.slds) {
                this.isGiant = true;
                this.isMini = false;
            } else {
                this.isGiant = false;
                this.isMini = true;
            }
            this.messageHeader = param.messageHeader;
            this.headerIconName = param.headerIconName;
            
            this.messageBody = param.messageBody;
            this.messageIconName = param.messageIconName;

            this.hasClose = param.hasClose;

            this.buildButtonSet(param);
            this.isDisplayed = true;

            this.footerClassName = 'suiModalControls';
            if(param.buttonShape) this.footerClassName += ' ' + param.buttonShape;
            if(param.buttonArrangement) this.footerClassName += ' ' + param.buttonArrangement;
            else this.footerClassName += ' center';
        }
        else this.util_throwError();
    }
    buildButtonSet(param) {
        switch(this.type) {
            case "yesno":
                this.buttonSet = buttonYesNo;
                break;
            case "close": 
                this.buttonSet = buttonClose;
                break;
            case "okay": 
                this.buttonSet = buttonOkay;
                break;
            case "cancelnext": 
                this.buttonSet = buttonCancelNext;
                break;
            case "list" : 
                this.buttonSet = param.buttonSet;
                break;
            default : 
        }
        this.buttonSet.forEach(b => {
            switch(b.type) {
                case "positive":
                    b.class = "slds-button slds-button_brand positive";
                    break;
                case "neutral":
                    b.class = "slds-button slds-button_neutral neutral";
                    break;
                case "negative":
                    b.class = "slds-button slds-button_destructive negative";
                    break;
                default:
                    b.class = "slds-button slds-button_neutral";
            }
        });
    }

    @api close() {
        this.anim_playClose();
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        window.setTimeout(() => { this.util_reset(true); }, 200);
    }

    util_paintButtons() {
        var buttonPositives = this.template.querySelector('.buttonPositive');
        var buttonNegatives = this.template.querySelector('.buttonNegative');
        var buttonNeutrals = this.template.querySelector('.buttonNeutral');
        var boxContainer = this.template.querySelector('.luiModalBody');
        var textContainer = this.template.querySelector('.luiModalMessageBody');
        console.log("buttons >>> ", buttonPositives, buttonNegatives, buttonNeutrals, boxContainer);
        
        if(buttonPositives) {
            if(Array.isArray(buttonPositives)) {
                buttonPositives.forEach(
                    (button) => { button.style = this.buttonPositiveStyle; }
                );
            } else buttonPositives.style = this.buttonPositiveStyle;
        }
        
        if(buttonNegatives) {
            if(Array.isArray(buttonNegatives)) {
                buttonNegatives.forEach(
                    (button) => { button.style = this.buttonNegativeStyle; }
                );
            } else buttonNegatives.style = this.buttonNegativeStyle;
        }
        
        if(buttonNeutrals) {
            if(Array.isArray(buttonNeutrals)) {
                buttonNeutrals.forEach(
                    (button) => { button.style = this.buttonNeutralStyle; }
                );
            } else buttonNeutrals.style = this.buttonNeutralStyle;
        }
        
        if(boxContainer) {
            if(Array.isArray(boxContainer)) {
                boxContainer.forEach(
                    (div) => { div.style = this.boxStyle; }
                );
            } else boxContainer.style = this.boxStyle;
        }
        
        if(textContainer) {
            if(Array.isArray(textContainer)) {
                textContainer.forEach(
                    (div) => { div.style = this.textStyle; }
                );
            } else textContainer.style = this.textStyle;
        }
        
    }
    util_throwError() {
        console.log("error");
    }

    util_getTopLevelClass() {
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            //this.isMobile = true;
            return 'suiModalBackdrop'; // mobile';
        } 
        return 'suiModalBackdrop';
    }
    util_reset(fullClose) {
        this.id = null;
        this.type = null;
        this.isYesNo = false;
        this.isClose = false;
        this.isSimple = false;
        this.isCustom = false;
        this.messageHeader = null;
        this.messageBody = null;
        this.hasClose = false;
        this.messageIconName = "utility:info_alt";
        this.messageHeader = "Message Header";
        this.messageBody = "Message Body";
        this.hasIcon = false;
        if(fullClose) this.isDisplayed = false;
        // eslint-disable-next-line @lwc/lwc/no-document-query
        if(this.preventScroll) document.getElementsByTagName("body")[0].style.overflow = "initial";
    }
    anim_playClose() {
        this.topLevelClassName = this.util_getTopLevelClass() + ' close';
    }
    onButtonClick(e) {
        var isClose = false;
        this.buttonSet.forEach(b => {
            if(e.target.name == b.label) {
                if(b.isClose) isClose = true;
            }
        });
        if(isClose) this.close();
        
        this.dispatchEvent(
            new CustomEvent(
                'buttonpress', 
                {detail : {id: this.id, pressed : e.target.name, passBack : this.passBack} }
            )
        );
        this.passBack = null;
    }
    onClosePage() {
        this.close();
    }

}